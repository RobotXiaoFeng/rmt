function s = defineTag(name, parent, r, R, visualize)
%%%
%
% Defines a tag, i.e., a frame of interest (Frame{TAG}).
%
% Input:
% ------
% name   - name of tag
% parent - parent frame (Frame{PARENT}) w.r.t which Frame{TAG} is defined
% r      - position of origin of Frame{TAG} as seen from Frame{PARENT}
% R      - rotation matrix defining the orientation offset from Frame{PARENT}
% visualize - (true)  plot connection with origin of local frame (default)
%             (false) don't plot connection
%
% Output:
% -------
% tag    - structure containing the tag definition
%
%%%

    if nargin < 5
	visualize = 1;
	if nargin < 4
	    R = eye(3);
	    if nargin < 3
		r = zeros(3,1);
	    end
	end
    end

    %% handle case defineTag(name, parent, r, [], visualize)
    if isempty(R)
	R = eye(3);
    end

    %% ---------------------------------------------------
    %% Constant parameters
    %% ---------------------------------------------------

    s.name = name;
    s.P    = parent;

    s.T_LOCAL = [R r(:); 0 0 0 1];

    s.visualize = visualize;

    %% ---------------------------------------------------
    %% Variable parameters (expressed in Frame{WORLD})
    %% ---------------------------------------------------

    s.T  = eye(4);      % homogeneous matrix to Frame{WORLD}

    s.v  = zeros(3,1);  % linear velocity of the origin
    s.dv = zeros(3,1);  % linear acceleration of the origin

    s.w  = zeros(3,1);  % angular velocity of the tag frame
    s.dw = zeros(3,1);  % angular acceleration of the tag frame

    s.Av = [];          % Jacobian (linear) w.r.t. motion of joints
    s.Bv = [eye(3), zeros(3,3)]; % Jacobian (linear) w.r.t. motion of base

%%%EOF
