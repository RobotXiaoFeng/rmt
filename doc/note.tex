\documentclass[12pt,a4paper]{article}

\newcommand{\norm}[1]{\lVert#1\rVert}

\usepackage{graphicx,color,psfrag}
\usepackage{tabularx,colortbl}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{epsfig}
\usepackage[sans]{dsfont} 
\usepackage{bbm} 
\usepackage{amsmath,bm}
\usepackage{verbatim}  
\usepackage{color}     
\usepackage{subfigure} 
\usepackage{hyperref}  
\usepackage{scalefnt}

\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}

\begin{document}

\section{Robot Modeling Tool (RMT) - some notes}

\subsection{Nomenclature}

Consider a system of $n+1$ rigid bodies (links) interconnected with $n$ rotational joints. All
vectors are assumed to be expressed with respect to the ``world frame'' (unless otherwise
specified).

\begin{itemize}
\item $c_k$ - position of the center of mass (CoM) of the $k$-th link.
\item $r_k$ - position of the origin of the body-fixed frame.
\item $x = (q,r_0,\theta_0) \in \mathbb{R}^{n+6}$ - the configuration of the system, where $\theta_0$ is a set of
  Euler angles ($e.g.,$ in order to be consistent with \verb9HuMAnS9, a $z\to y\to x$ sequence around
  the new axis is used in the model of \verb9HRP29) and $q \in \mathbb{R}^{n}$ denotes the joint angles. 
\item $(x,\dot{x}) \in \mathbb{R}^{2(n+6)}$ - the state of the system, where $\dot{\theta}_0$ denotes the rates of change of $\theta_0$.
\item $\mathbb{I}_k$ - inertia matrix of the $k$-th link around its CoM expressed in the body-fixed frame (assumed to be constant).
\item $\mathcal{I}_k$ - inertia matrix of the $k$-th link around its CoM. Note that $\mathcal{I}_k =
  R_k\mathbb{I}_kR_k^T$, where $R_k$ is a rotation matrix from the local frame fixed in link $k$ to the world
  frame.
\item $m_k$ - the mass of link $k$ (assumed to be constant).
\item $v_k = \dot{r}_k$ - $i.e.,$ the linear velocity of the origin of the body-fixed frame.
\item $\omega_k$ - angular velocity of link $k$.
\item $s_k$ - axis of rotation of link $k$.
\item $F_k$ - force acting at $c_k$.
\item $T_k$ - torque acting at link $k$.
\item $\tilde{a}$ - a skew-symmetric matrix associated with the vector $a\in \mathbb{R}^{3}$, hence $a\times b = \tilde{a}b$.
\end{itemize}

\subsection{Equations of motion}

The equations of motion of the $k$-th link (considering it in isolation) are
%
\begin{align}
\underbrace{\left[\begin{array}{cc} m_kI & 0 \\ 0 & \mathcal{I}_k \end{array}\right]}_{M_k}
\left[\begin{array}{c} \ddot{c}_k \\ \dot{\omega}_k \end{array}\right] + 
\underbrace{\left[\begin{array}{c} 0 \\ \tilde{\omega}_k\mathcal{I}_k\omega_k \end{array}\right]}_{\bar{h}_k} = 
\underbrace{\left[\begin{array}{c} F_k \\ T_k \end{array}\right]}_{W_k}. \nonumber
\end{align}
%
Let $c_k$ be related to $x$ as
%
\[
c_k = f_k(x), \quad \mbox{where} \ \ f_k: \mathbb{R}^{n+6} \to \mathbb{R}^{3}.
\]
%
Hence,
%
\[
\dot{c}_k = \underbrace{\frac{\partial f_k}{\partial x}}_{J_{c_k}}\dot{x}, \quad 
\ddot{c}_k = J_{c_k}\ddot{x} + \dot{J}_{c_k}\dot{x}.
\]
%
Let $\omega_k = J_{\omega_k}\dot{x}$ and $\dot{\omega}_k = J_{\omega_k}\ddot{x} + \dot{J}_{\omega_k}\dot{x}$ for an
appropriately chosen matrix $J_{\omega_k}$. Using 
%
\[
J_k = \left[\begin{array}{c} J_{c_k} \\ J_{\omega_k} \end{array}\right], \quad
\xi_k = \left[\begin{array}{c} \dot{c}_k \\ \omega_k \end{array}\right], \quad
\dot{\xi}_k = \left[\begin{array}{c} \ddot{c}_k \\ \dot{\omega}_k \end{array}\right],
\]
%
\[
J = \left[\begin{array}{c} J_0 \\ \vdots \\ J_{n} \end{array}\right], \quad
%
\bar{h} = \left[\begin{array}{c} \bar{h}_0 \\ \vdots \\ \bar{h}_{n} \end{array}\right], \quad
%
W = \left[\begin{array}{c} W_0 \\ \vdots \\ W_{n} \end{array}\right],
\]
%
the equations of motion of all links (assuming no joints interconnecting them) can be stated as
%
\[
M\dot{\xi} + \bar{h} = W,
\]
%
where $M\in\mathbb{R}^{(n+6)\times(n+6)}$ is a block-diagonal matrix with $M_k$ ($k=0,\dots,n$) on
its diagonal.
%
Substituting $\dot{\xi} = J\ddot{x} + \dot{J}\dot{x}$ above and pre-multiplying by $J^T$, leads to 
%
\[
\underbrace{J^TMJ}_{H}\ddot{x} + 
\underbrace{J^T(M\dot{J}\dot{x}+\bar{h})}_{h} = J^{T}W.
\]
%
Assuming that the base link is not actuated, $i.e.,$ $F_0 = T_0 = 0$, the above equation becomes
%
\[
H\underbrace{\left[\begin{array}{c} \ddot{q} \\ \ddot{r}_0 \\ \ddot{\theta}_0 \end{array}\right]}_{\ddot{x}} + h = \left[\begin{array}{c} \tau_a \\ 0 \end{array}\right],
\]
%
where $\tau_a$ includes all torques acting in the generalized coordinates (except for the inertial
forces/torques, which are summarized in $h\in\mathbb{R}^{n+6}$). $H\in\mathbb{R}^{(n+6)\times
  (n+6)}$ is commonly referred to as the (generalized) inertia matrix of the system and is symmetric
and positive-definite. Note that $H = H(x)$ and $h = h(x,\dot{x})$. Alternatively, $h$ could be
represented in the form $h = \hat{h}(x,\dot{x})\dot{x}$, for an appropriately chosen $\hat{h} \in
\mathbb{R}^{(n+6)\times(n+6)}$.

\subsubsection{Angular velocity $\omega$ in terms of $\dot{\theta}_0$}

Consider for example a $z \to y \to x$ sequence of Euler angles (around the new axis), i.e.,
%
\[
R = R_z(\gamma)R_y(\beta)R_x(\alpha),
\]
%
where
%
\[
R_x(\alpha) = \left[\begin{array}{ccc} 
    1 & 0 & 0 \\
    0 & c_{\alpha} & -s_{\alpha} \\
    0 & s_{\alpha} &  c_{\alpha} 
\end{array}\right], \quad
%
R_y(\beta) = \left[\begin{array}{ccc} 
    c_{\beta} & 0 & s_{\beta} \\
    0 & 1 & 0 \\
    -s_{\beta} & 0 & c_{\beta} 
\end{array}\right], \quad
%
R_z(\gamma) = \left[\begin{array}{ccc} 
    c_{\gamma} & -s_{\gamma} & 0 \\
    s_{\gamma} &  c_{\gamma} & 0 \\
    0 & 0 & 1
\end{array}\right].
\]
%
The angular velocity (in the world-frame) $w$ is related to the angular rates
$(\dot{\alpha},\dot{\beta},\dot{\gamma})$ using
%
\[
w = 
\underbrace{\left[\begin{array}{ccc} e_z & R_z(\gamma)e_y & R_z(\gamma)R_y(\beta)e_x \end{array}\right]}_{E}
\left[\begin{array}{c} \dot{\gamma} \\ \dot{\beta} \\ \dot{\alpha} \end{array}\right],
\]
%
where
%
\[
e_x = \left[\begin{array}{c} 1 \\ 0 \\ 0 \end{array}\right], \quad
e_y = \left[\begin{array}{c} 0 \\ 1 \\ 0 \end{array}\right], \quad
e_z = \left[\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right], \quad
%
E = \left[\begin{array}{ccc} 
0 & -s_{\gamma} & c_{\beta}c_{\gamma} \\
0 & c_{\gamma} & c_{\beta}s_{\gamma} \\
1 & 0 & -s_{\beta}. 
\end{array}\right].
\]

\subsubsection{Angular acceleration $\dot{\omega}$ in terms of $\ddot{\theta}_0$ and $\dot{\theta}_0$}

\[
\dot{\omega} = E\ddot{\theta}_0 + \dot{E}\dot{\theta}_0,
\]
%
where $\dot{E}$ can be computed using
%
\[
\dot{E} = \frac{\partial E}{\partial \alpha}\dot{\alpha} + 
\frac{\partial E}{\partial \beta}\dot{\beta} + \frac{\partial E}{\partial \gamma}\dot{\gamma} \in \mathbb{R}^{3 \times 3}.
\]

\subsubsection{Jacobian matrices}

\begin{align}
\left[\begin{array}{c} \dot{c}_k^{(b)} \\ \omega_k^{(b)} \end{array}\right] 
&= \left[\begin{array}{cc} I & -\tilde{r}_k \\ 0 & I \end{array}\right]
\left[\begin{array}{c} \dot{r}_0 \\ \omega_0 \end{array}\right] \nonumber \\
%
&= \underbrace{\left[\begin{array}{cc} I & -\tilde{r}_kE \\ 0 & E \end{array}\right]}_{B_k}
\left[\begin{array}{c} \dot{r}_0 \\ \dot{\theta}_0 \end{array}\right], \nonumber
\end{align}
%
where $r_k = (c_k - x_0)$, and $\dot{c}_k^{(b)}$, $\omega_k^{(b)}$ are the linear velocity of the
CoM of link $k$ and the angular velocity of link $k$ as a result of the motion of the base.
%
\[
\left[\begin{array}{c} \dot{c}_k^{(m)} \\ \omega_k^{(m)} \end{array}\right] = A_{k}\dot{q},
\]
%
where $\dot{c}_k^{(m)}$, $\omega_k^{(m)}$ are the linear velocity of the CoM of link $k$ and the
angular velocity of link $k$ as a result of the motion of the manipulator (without its base).  $A_k$
is commonly refered to as a ``geometric'' Jacobian. Hence,
%
\[
\left[\begin{array}{c} \dot{c}_k \\ \omega_k \end{array}\right] = 
\left[\begin{array}{c} \dot{c}_k^{(b)} \\ \omega_k^{(b)} \end{array}\right] + 
\left[\begin{array}{c} \dot{c}_k^{(m)} \\ \omega_k^{(m)} \end{array}\right] = 
%
A_k\dot{q} + B_k\left[\begin{array}{c} \dot{r}_0 \\ \dot{\theta}_0 \end{array}\right], \quad \mbox{and} \ \ 
%
J_k = \left[\begin{array}{cc} A_k & B_k \end{array}\right].
\]

\subsubsection{Computation of $h$}

Forming $h$ requires the computation of $\dot{J}\dot{x}$. One way of doing this is to compute
$\dot{\xi}$ resulting from zero $\ddot{x}$, $i.e.,$
%
\[
\dot{\xi} = \underbrace{J\ddot{x}}_{0} + \dot{J}\dot{x}.
\]
%
Hence, one has to compute $\ddot{c}_k$ and $\dot{\omega}_k$ for all $k$ (which can be achieved using
a simple recursion).

Note that even though $\ddot{\theta_0} = 0$ is assumed, this does not mean that $\dot{\omega}_0 =
0$. $\dot{\omega}_0$ should be initialized equal to $\dot{E}\dot{\theta_0}$.

\end{document}

%%%EOF
