function r_local = global2local(MODEL,parent,r)
%%%
%
% given a vector r in the global frame, return
% the corresponding vector in the Frame of parent
%
%%%

    r_local = MODEL.Frame(parent).T\[r;1];
    r_local = r_local(1:3);

%%%EOF
