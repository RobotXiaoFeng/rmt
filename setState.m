function MODEL = setState(MODEL, x, dx)
%%%
%
% Sets the state (x,dx) of a MODEL and updates it
%
%%%

    n = MODEL.n;

    if nargin < 3
	dx = zeros(n+6,1);
    end

    %% -----------------------------------
    if length(x) ~= n+6
	x = [x;zeros(6,1)];
    end

    if length(dx) ~= n+6
	dx = [dx;zeros(6,1)];
    end
    %% -----------------------------------

    MODEL.x  = x;
    MODEL.dx = dx;

    F  = MODEL.Frame(1);

    %% -----------------------------------
    %% handle the base
    %% -----------------------------------
    r0  = x(n+1:n+3);
    dr0 = dx(n+1:n+3);
    e0  = x(n+4:n+6);
    de0 = dx(n+4:n+6);

    F.T(1:3,1:3) = e2R(e0);
    F.T(1:3,4)   = r0(1:3);

    [E, dE] = dde2dw(e0,de0);
    F.v =   dr0;
    F.w = E*de0;
    %% -----------------------------------

    MODEL.E  = E;
    MODEL.dE = dE;
    MODEL.Frame(1) = F; % save

    MODEL = updateFrame(MODEL);
    MODEL = updateDP(MODEL);
    MODEL = updateTag(MODEL);

%%%EOF
