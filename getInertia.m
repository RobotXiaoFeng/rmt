function [H,h] = getInertia(MODEL, Gravity)
%%%
%
% Forms the inertia matrix H and inertial forces h (including forces due to Gravity)
%
%%%

    if nargin < 2
	Gravity = zeros(3,1);
    end

    n = MODEL.n;

    J = zeros(6*n+6,n+6);
    M = zeros(6*n+6,6*n+6);
    f = zeros(6*n+6,1);

    for iL = 1:n+1 % iterate over all links
	F   = MODEL.Frame(iL);
	D   = MODEL.DP(iL);
	ind = 6*iL-5:6*iL;

	J(ind,:) = [D.Av, D.Bv;
		    F.Aw, F.Bw];

	M(ind,ind) = [eye(3)*D.m, zeros(3,3);
		      zeros(3,3),       D.I];

	f(ind) = [D.m*(D.ddc - Gravity);
		  D.I*F.dw + tilde(F.w)*D.I*F.w];
    end

    H = J'*M*J;
    h = J'*f;

%%%EOF
