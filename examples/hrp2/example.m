%
% Form the inertia matrix H using n+6 calls to getInertialForces.m
%

addpath('~/local/opt/rmt')

clear;clc

% =====================================================

hrp2 = hrp2Model();

x    = InitialConfigurations(2);
dx   = zeros(hrp2.n+6,1);

hrp2 = setState(hrp2,x,dx);
H    = getInertia(hrp2);

% =====================================================

for i=1:hrp2.n+6
  hrp2.ddx(i) = 1;
  hrp2 = setState(hrp2, x);
  [~, H1(:,i)] = getInertia(hrp2);
  hrp2.ddx(i) = 0;
end

fprintf('norm(H-H1) = %e\n', norm(H-H1))

drawModel(hrp2); axis equal; grid on; view(3); axis tight

[J,dJdx] = getJacobian(hrp2,10); % get the Jacobian of tag 10

%%%EOF
