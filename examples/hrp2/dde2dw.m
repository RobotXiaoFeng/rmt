function [E,dE] = dde2dw(e,de)
  
  x_axis = [1;0;0];
  y_axis = [0;1;0];
  z_axis = [0;0;1];
  
  x  = e(1); % not used
  y  = e(2);
  z  = e(3);
  
  dx = de(1);
  dy = de(2);
  dz = de(3);
  
  E  = [rz(z)*ry(y)*x_axis, rz(z)*y_axis, z_axis];
  
  dE = [-dy*cos(z)*sin(y)-dz*cos(y)*sin(z), -dz*cos(z),  0;
	 dz*cos(y)*cos(z)-dy*sin(y)*sin(z), -dz*sin(z),  0;
	                        -dy*cos(y),          0,  0];

end
