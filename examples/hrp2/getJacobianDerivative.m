function dJ = getJacobianDerivative(MODEL,x,dx,tag_num)
%
% form dJ
%

dA = zeros(6,MODEL.n);
dB = zeros(6,6);

% ----------------------------------------------------------------

tag   = MODEL.Tag(tag_num);
frame = MODEL.Frame(tag.P); 

BasePosition = MODEL.Frame(1).T(1:3,4);
BaseVelocity = MODEL.Frame(1).v;
TagPosition  = tag.T(1:3,4);

% ----------------------------------------------------------------

for k=1:length(frame.joints)
  JointIndex = frame.joints(k);
  FrameIndex = JointIndex + 1;
  
  JointPosition = MODEL.Frame(FrameIndex).T(1:3,4);
  JointAxis     = MODEL.Frame(FrameIndex).ax;
  w = MODEL.Frame(FrameIndex).w;
  v = MODEL.Frame(FrameIndex).v;
  r = TagPosition - JointPosition;

  dA(1:3,JointIndex) = tilde(tilde(w)*JointAxis)*r + tilde(JointAxis)*(tag.v - v);
  dA(4:6,JointIndex) = tilde(w)*JointAxis;
end
dB(4:6,4:6) = MODEL.dE;
dB(1:3,4:6) = -tilde(tag.v - BaseVelocity)*MODEL.E - tilde(TagPosition - BasePosition)*MODEL.dE;

% ----------------------------------------------------------------

dJ = [dA, dB];

%%%EOF