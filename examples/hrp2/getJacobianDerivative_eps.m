function dJ_eps = getJacobianDerivative_eps(MODEL,x,dx,tag_num)
%
% form dJ numerically
%

epsilon = sqrt(eps);

dJ_eps = zeros(6,MODEL.n+6);

J = getJacobian(MODEL,tag_num);

% ----------------------------------------------

for i=1:MODEL.n+6   
  x_eps    = x;
  x_eps(i) = x_eps(i) + epsilon;

  MODEL = setState(MODEL,x_eps,dx);

  J_eps = getJacobian(MODEL,tag_num);
  dJ_eps = dJ_eps + dx(i)*(J_eps - J)/epsilon;
end

%%%EOF