function MODEL = hrp2Model()
%%%
%
% HRP2 model (addopted from HuMAnS, there are differences from the model in HRP2JRLmain.wrl)
%
% Numberig of joins (the robot is facing us):
%
%                                  Head
%
%                                   16
%                                   15
%                    19 18 17 ----- 14 ----- 24 25 26
%                    20             13             27
%                    21          1       7         28
%                    22          2       8         29
%                  23            3       9           30
%                                4       10
%                                5       11
%                                6       12
%
%                              Right    Left
%                               leg      leg
%
%%%

    %% -------------------------------------------------------------------------------------------
    %% Definition of FRAMES
    %% -------------------------------------------------------------------------------------------

    %% BASE
    MODEL.Frame(1)  = defineFrame('waist');

    %% RIGHT LEG
    MODEL.Frame(2)  = defineFrame(       'Right hip YAW',  1, [0;0;1], [     0; -0.06;      0]);
    MODEL.Frame(3)  = defineFrame(      'Right hip ROLL',  2, [1;0;0], [     0;     0;      0]);
    MODEL.Frame(4)  = defineFrame(     'Right hip PITCH',  3, [0;1;0], [     0;     0;      0]);
    MODEL.Frame(5)  = defineFrame(    'Right knee PITCH',  4, [0;1;0], [     0;     0;   -0.3]);
    MODEL.Frame(6)  = defineFrame(   'Right ankle PITCH',  5, [0;1;0], [     0;-0.035;   -0.3]);
    MODEL.Frame(7)  = defineFrame(    'Right ankle ROLL',  6, [1;0;0], [     0;     0;      0]);

    %% LEFT LEG
    MODEL.Frame(8)  = defineFrame(        'Left hip YAW',  1, [0;0;1], [     0;  0.06;      0]);
    MODEL.Frame(9)  = defineFrame(       'Left hip ROLL',  8, [1;0;0], [     0;     0;      0]);
    MODEL.Frame(10) = defineFrame(      'Left hip PITCH',  9, [0;1;0], [     0;     0;      0]);
    MODEL.Frame(11) = defineFrame(     'Left knee PITCH', 10, [0;1;0], [     0;     0;   -0.3]);
    MODEL.Frame(12) = defineFrame(    'Left ankle PITCH', 11, [0;1;0], [     0; 0.035;   -0.3]);
    MODEL.Frame(13) = defineFrame(     'Left ankle ROLL', 12, [1;0;0], [     0;     0;      0]);

    %% CHEST
    MODEL.Frame(14) = defineFrame(           'Chest YAW',  1, [0;0;1], [ 0.032;     0; 0.3507]);
    MODEL.Frame(15) = defineFrame(         'Chest PITCH', 14, [0;1;0], [     0;     0;      0]);

    %% HEAD
    MODEL.Frame(16) = defineFrame(            'Head YAW', 15, [0;0;1], [-0.007;     0; 0.2973]);
    MODEL.Frame(17) = defineFrame(          'Head PITCH', 16, [0;1;0], [     0;     0;      0]);

    %% RIGHT ARM
    MODEL.Frame(18) = defineFrame('Right shoulder PITCH', 15, [0;1;0], [ 0.008; -0.25;  0.181]);
    MODEL.Frame(19) = defineFrame( 'Right shoulder ROLL', 18, [1;0;0], [     0;     0;      0]);
    MODEL.Frame(20) = defineFrame(  'Right shoulder YAW', 19, [0;0;1], [     0;     0;      0]);
    MODEL.Frame(21) = defineFrame(   'Right elbow PITCH', 20, [0;1;0], [     0;     0;  -0.25]);
    MODEL.Frame(22) = defineFrame(     'Right wrist YAW', 21, [0;0;1], [     0;     0;  -0.25]);
    MODEL.Frame(23) = defineFrame(   'Right wrist PITCH', 22, [0;1;0], [     0;     0;      0]);
    MODEL.Frame(24) = defineFrame(  'Right finger PITCH', 23, [0;1;0], [     0;  0.02; -0.095]);

    %% LEFT ARM
    MODEL.Frame(25) = defineFrame( 'Left shoulder PITCH', 15, [0;1;0], [ 0.008;  0.25;  0.181]);
    MODEL.Frame(26) = defineFrame(  'Left shoulder ROLL', 25, [1;0;0], [     0;     0;      0]);
    MODEL.Frame(27) = defineFrame(   'Left shoulder YAW', 26, [0;0;1], [     0;     0;      0]);
    MODEL.Frame(28) = defineFrame(    'Left elbow PITCH', 27, [0;1;0], [     0;     0;  -0.25]);
    MODEL.Frame(29) = defineFrame(      'Left wrist YAW', 28, [0;0;1], [     0;     0;  -0.25]);
    MODEL.Frame(30) = defineFrame(    'Left wrist PITCH', 29, [0;1;0], [     0;     0;      0]);
    MODEL.Frame(31) = defineFrame(   'Left finger PITCH', 30, [0;1;0], [     0; -0.02; -0.095]);

    %% -------------------------------------------------------------------------------------------
    %% Definition of DYNAMIC PARAMETERS
    %% -------------------------------------------------------------------------------------------

    p = hrp2DP();
    for i = 1:length(p)
	MODEL.DP(i) = defineDP(p(i).c, p(i).m, p(i).I);
    end

    %% -------------------------------------------------------------------------------------------
    %% Definition of TAGS
    %% -------------------------------------------------------------------------------------------

    %% general tags
    MODEL.Tag(1)  = defineTag(      'Waist', 1);
    MODEL.Tag(2)  = defineTag('Right ankle', 7);
    MODEL.Tag(3)  = defineTag( 'Left ankle', 13);
    MODEL.Tag(4)  = defineTag('Right wrist', 23);
    MODEL.Tag(5)  = defineTag( 'Left wrist', 30);

    %% contact right foot (Right ankle ROLL)
    parent = 7;
    MODEL.Tag(6)  = defineTag('Right foot contact point 1', parent, [ 135.6,  59, -105]'*1e-3);
    MODEL.Tag(7)  = defineTag('Right foot contact point 2', parent, [ 135.6, -79, -105]'*1e-3);
    MODEL.Tag(8)  = defineTag('Right foot contact point 3', parent, [-105.6,  59, -105]'*1e-3);
    MODEL.Tag(9)  = defineTag('Right foot contact point 4', parent, [-105.6, -79, -105]'*1e-3);

    %% contact left foot (Left ankle ROLL)
    parent = 13;
    MODEL.Tag(10) = defineTag( 'Left foot contact point 1', parent, [ 135.6, -59, -105]'*1e-3);
    MODEL.Tag(11) = defineTag( 'Left foot contact point 2', parent, [ 135.6,  79, -105]'*1e-3);
    MODEL.Tag(12) = defineTag( 'Left foot contact point 3', parent, [-105.6, -59, -105]'*1e-3);
    MODEL.Tag(13) = defineTag( 'Left foot contact point 4', parent, [-105.6,  79, -105]'*1e-3);

    %% head
    parent = 17;
    MODEL.Tag(14) = defineTag(                    'Head 1', parent, [     0, -80,   40]*1e-3);
    MODEL.Tag(15) = defineTag(                    'Head 2', parent, [     0,  80,   40]*1e-3);
    MODEL.Tag(16) = defineTag(                    'Head 3', parent, [     0, -80,  200]*1e-3);
    MODEL.Tag(17) = defineTag(                    'Head 4', parent, [     0,  80,  200]*1e-3);

    %% define in order to plot connections between pairs of tags
    MODEL.TagPairs = [6,7;
		      6,8;
		      8,9;
		      7,9;
		      10,11;
		      10,12;
		      13,12;
		      13,11;
		      14,16;
		      16,17;
		      17,15];

    %% -------------------------------------------------------------------------------------------
    %% Definition of STATE VARIABLES
    %% -------------------------------------------------------------------------------------------
    n = length(MODEL.Frame) - 1; % number of joints

    MODEL.n   = n;
    MODEL.x   = zeros(n+6,1);
    MODEL.dx  = zeros(n+6,1);
    MODEL.ddx = zeros(n+6,1);

    MODEL.E   = eye(3);
    MODEL.dE  = zeros(3,3);
    MODEL.CoM = zeros(3,1);

%%%EOF
