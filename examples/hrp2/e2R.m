function R = e2R(e)
%
% Form a rotation matrix given a set of Euler angles.
%
  
  R = rz(e(3))*ry(e(2))*rx(e(1)); % z-y-x around new axis 
  
end
