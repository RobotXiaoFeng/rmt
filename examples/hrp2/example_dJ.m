%
% example form dJ
%

addpath('~/local/opt/rmt')

clear;clc

% =====================================================

hrp2 = hrp2Model();

x    = InitialConfigurations(1);
dx   = randn(hrp2.n+6,1);

hrp2 = setState(hrp2,x,dx);

% =====================================================
% verify [Av, Bv] using numerical differentiation
% =====================================================


for i = 1:length(hrp2.Tag);

  dJ     = getJacobianDerivative(hrp2,x,dx,i);
  dJ_eps = getJacobianDerivative_eps(hrp2,x,dx,i);

  err1(i) = norm(dJ - dJ_eps);

  [~,dJdx] = getJacobian(hrp2,i);

  err2(i) = norm(dJ*dx - dJdx);

  fprintf('tag number = %d, err = [%e, %e] \n',i,err1(i),err2(i));

end

%%%EOF
