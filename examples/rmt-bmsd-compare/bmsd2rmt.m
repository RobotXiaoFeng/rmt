function MODEL = bmsd2rmt(SP)
%%%
%
% convet a bmsd model (in SP) to a rmt model (MODEL)
%
%%%

    %% -------------------------------------------------------------------------------------------
    %% Definition of FRAMES
    %% -------------------------------------------------------------------------------------------

    %% BASE
    MODEL.Frame(1) = defineFrame('base');

    for i=2:SP.n+1
	input_joint = i-1;

	a = SP.J(input_joint).rpy;
	R = rx(a(1)) * ry(a(2)) * rz(a(3)); % rpy2R(SP.J(input_joint).rpy);
	r = SP.J(input_joint).t;

	if SP.C(i) ~= 1 %% if the parent link is not the base
	    parent_input_joint = SP.C(i)-1;
	    r = r + SP.J(parent_input_joint).f;
	end

	MODEL.Frame(i) = defineFrame(['link',num2str(i)], SP.C(i), [0;0;1], r, R);
    end

    %% -------------------------------------------------------------------------------------------
    %% Definition of DYNAMIC PARAMETERS
    %% -------------------------------------------------------------------------------------------

    MODEL.DP(1) = defineDP([0;0;0], SP.L(1).m, SP.L(1).I);
    for i = 2:SP.n+1
	input_joint = i-1;
	MODEL.DP(i) = defineDP(SP.J(input_joint).f, SP.L(i).m, SP.L(i).I);
    end

    %% -------------------------------------------------------------------------------------------
    %% Definition of TAGS
    %% -------------------------------------------------------------------------------------------

    for i=1:length(SP.bN)
	input_joint = SP.bN(i)-1;
	MODEL.Tag(i) = defineTag('end-effector', SP.bN(i), SP.bP(:,i) + SP.J(input_joint).f);
    end

    %% -------------------------------------------------------------------------------------------
    %% Definition of STATE VARIABLES
    %% -------------------------------------------------------------------------------------------
    n = length(MODEL.Frame) - 1; % number of joints

    MODEL.n   = n;
    MODEL.x   = zeros(n+6,1);
    MODEL.dx  = zeros(n+6,1);
    MODEL.ddx = zeros(n+6,1);

    MODEL.E   = eye(3);
    MODEL.dE  = zeros(3,3);
    MODEL.CoM = zeros(3,1);

%%%EOF
