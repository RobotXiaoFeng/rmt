function varargout = get_labels(varargin)
%%%
%
% Example:
% ----------
% n joint {positions, velocities} => {x(JOINTS), dx(JOINTS)}
% 6 base  {positions, velocities} => {x(BASE)  , dx(BASE)  }
% n joint {torques}
%
% [JOINTS, BASE, TAU] = get_labels(n,6,n);
%
%%%

    ind_start = 0;
    ind_end   = 0;
    for i=1:length(varargin)
	ind_start    = ind_end;
	ind_end      = ind_start + varargin{i};
	varargout{i} = ind_start+1:ind_end;
    end

%%%EOF
