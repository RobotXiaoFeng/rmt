function s = planarDP(n)
%%%
%
% dynamic parameters of n DoF planar manipulator
%
%%%

    for j = 1:n+1;
	s(j).m = 1;
	s(j).c = zeros(3,1);
	s(j).I = eye(3);
    end

%%%EOF
