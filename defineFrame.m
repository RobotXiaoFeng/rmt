function s = defineFrame(name, parent, ax, r, R)
%%%
%
% Defines a right-handed frame (Frame{CURRENT}) whose origin coincides with the input joint of a link
%
% Input:
% ------
% name   - name of frame
% parent - parent frame (Frame{PARENT})
% ax     - joint axis of rotation expressed in Frame{CURRENT} (unit length assumed)
% r      - position of origin of Frame{CURRENT} as seen from Frame{PARENT}
% R      - rotation matrix defining the orientation offset from Frame{PARENT}
%
% Output:
% -------
% s      - structure containing the frame definition
%
% Note 1: if x is expressed in Frame{CURRENT}, then [y;1] = [R r; 0 0 0 1]*[x;1] is expressed in Frame{PARENT}
% Note 2: variable-names ending with _LOCAL are constant in some local frame of reference
% Note 3: when defining the base link one has to use ax = []; r = zeros(3,1), R = eye(3)
%
%%%

    if nargin < 5
	R = eye(3);
	if nargin < 4
	    r = zeros(3,1);
	    if nargin < 3
		ax = [];
		if nargin < 2
		    parent = 0;
		end
	    end
	end
    end

    %% ---------------------------------------------------
    %% Constant parameters
    %% ---------------------------------------------------

    s.name   = name;
    s.P      = parent;
    s.joints = []; % joints that support a frame

    s.ax_LOCAL = ax;
    s.T_LOCAL  = [R r(:); 0 0 0 1];

    %% ---------------------------------------------------
    %% Variable parameters (expressed in Frame{WORLD})
    %% ---------------------------------------------------

    s.ax = zeros(3,1); % axis of rotation
    s.T  = eye(4);     % homogeneous matrix to Frame{WORLD}

    s.v  = zeros(3,1); % linear velocity of the origin
    s.dv = zeros(3,1); % linear acceleration of the origin

    s.w  = zeros(3,1); % angular velocity of frame
    s.dw = zeros(3,1); % angular acceleration of frame

    s.Aw = [];         % Jacobian (angular) w.r.t. motion of joints
    s.Bw = zeros(3,6); % Jacobian (angular) w.r.t. motion of base

%%%EOF
