function drawModel(MODEL, opt)
%%%
%
% Displays MODEL (opt: see defineDrawOptions.m)
%
%%%

    if nargin < 2
	opt = defineDrawOptions();
    end

    n  = MODEL.n;           % number of joints
    nT = length(MODEL.Tag); % number of tags

    hold on
    for iL = 1:n+1 % iterate over all links
	L = MODEL.Frame(iL);

	r = L.T(1:3,4);
	plot3(r(1),r(2),r(3),[opt.color_joints,'o'],'MarkerSize',opt.size_joint); % plot joints

	if iL > 1
	    rP = MODEL.Frame(L.P).T(1:3,4);
	    drawLineSegment(rP,r,opt.color_link,opt.line_width); % plot joint connections
	end

	%% draw masses
	if opt.draw_com
	    c = MODEL.DP(iL).c;
	    plot3(c(1),c(2),c(3),[opt.color_com,'.'],'MarkerSize',opt.size_com); % plot CoM
	    drawLineSegment(r,c,opt.color_com); % plot CoM connections
	end

	%% draw frames
	if opt.draw_frame
	    drawFrame(L.T);
	end
    end

    for iT = 1:nT % iterate over all tags
	tag = MODEL.Tag(iT);

	r0 = MODEL.Frame(tag.P).T(1:3,4);
	r1 = tag.T(1:3,4);

	plot3(r1(1),r1(2),r1(3),[opt.color_tag,'.'],'MarkerSize',opt.size_tag); % plot tag point

	if opt.draw_tag_num
	    text(r1(1),r1(2)+0.03,r1(3),num2str(iT),'FontSize',opt.size_tag_num); % plot tag number
	end

	if tag.visualize
	    drawLineSegment(r0,r1,opt.color_tag,opt.line_width); % plot tag connections
	end
    end

    %% draw connections between given pairs of tags: TagPairs(i,:) contains the i-th pair of tags
    if isfield(MODEL,'TagPairs')
	for i = 1:size(MODEL.TagPairs,1)
	    j = MODEL.TagPairs(i,1);
	    k = MODEL.TagPairs(i,2);
	    rj = MODEL.Tag(j).T(1:3,4);
	    rk = MODEL.Tag(k).T(1:3,4);

	    drawLineSegment(rj,rk,[opt.color_tag,'--']);
	end
    end

%%%EOF
