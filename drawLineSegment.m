function drawLineSegment(p1,p2,color,lw)
%%%
%
% Plots a line segment between two points
%
% p1    - first point
% p2    - second point
% color - color of line segment
% lw    - line width
%
%%%

    if nargin < 4
	lw = 1;
	if nargin < 3
	    color = 'k';
	end
    end

    plot3([p1(1) p2(1)],[p1(2) p2(2)],[p1(3) p2(3)],color,'LineWidth',lw);

%%%EOF
