This is a tool for modelling of multi-body systems with a tree structure and a moving base (the
implementation is not meant to be efficient).

1. Euler angles are used to describe the orientation of the base link. The type of Euler angles to
   be used has to be specified by the user in e2R.m.

2. The velocity and acceleration of base rotation can be specified using either (see ./doc/note.pdf):
   (1) rates of the Euler angles (define dde2dw.m according to your choice in e2R.m)
   (2) angular velocity and acceleration (if the output of dde2dw.m is E = eye(3), dE = zeros(3,3))

2. The reference point on the base need not coincide with its CoM (as was the case in bMSd - which
   was very inconvenient).

3. An arbitrary local axis of rotation can be specified (using only the local z axis requires
   additional frame rotations wich were very inconvenient in bMSd).

4. All accelerations Frame(k).dv, Frame(k).dw, DP(k).ddc, Tag(j).dv, Tag(j).dw are computed with
   zero accelerations of the generalized coordinates. They are used to compute, e.g., dJ*dx in ddc =
   J*ddx + dJ*dx.

5. The workflow is:
   - define a model M
   - set the state of M
     this updates the posture, velocities and accelerations of
     	  - frame in each link
   	  - tags
	  - CoM
     in addition Jacobian matrices are formed
   - then one can use, e.g., getInertia(M)

6. "tags" define points of interest (e.g., end-effector)

7. Note that the order of state variables is different from the one in bMSd
   rmt : (joint-variables, base-variables)
   bMSd: (base-variables, joint-variables)
