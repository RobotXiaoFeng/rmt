function Ry = ry(A)
%%%
%
% rotation matrix (Y axis)
%
%%%

    Ry = [cos(A) 0 sin(A);
	  0 1 0;
	  -sin(A) 0 cos(A)];

%%%EOF
